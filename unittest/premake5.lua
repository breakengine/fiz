project "unittest"
	language "C++"
	kind "ConsoleApp"

	files
	{
		"unittest_main.cpp",
		"unittest_fiz.cpp"
	}

	includedirs
	{
		"Catch2/single_include",
		"%{mn}/include",
		"%{hml}/include",
		"../include"
	}

	links
	{
		"mn",
		"fiz"
	}

	cppdialect "c++17"
	systemversion "latest"

	filter "system:linux"
		defines { "OS_LINUX" }

	filter "system:windows"
		defines { "OS_WINDOWS" }