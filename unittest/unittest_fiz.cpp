#include <catch2/catch.hpp>

#include <break/fiz/Buffer.h>
#include <break/fiz/Encoder.h>
#include <break/fiz/Decoder.h>

using namespace brk::fiz;

TEST_CASE("buffer", "[fiz]")
{
	Buffer b = buffer_new();

	buffer_push_uint(b, 123);
	buffer_push_int(b, -1235345);
	buffer_push_bool(b, true);
	buffer_push_float(b, 1.234);
	buffer_push_str(b, "Mostafa");

	mn::memory_stream_cursor_to_start(b);

	CHECK(buffer_pop_uint(b) == 123);
	CHECK(buffer_pop_int(b) == -1235345);
	CHECK(buffer_pop_bool(b) == true);
	CHECK(buffer_pop_float(b) == 1.234);
	CHECK(buffer_pop_str(b, mn::allocator_tmp()) == "Mostafa");

	buffer_free(b);
}

struct Point
{
	hml::vec2f v;
};

inline static void
encode_struct(Encoder en, const Point& p)
{
	encoder_struct_begin(en, "Point");
		encoder_field(en, "v", p.v);
	encoder_struct_end(en);
}

inline static void
decode_struct(Decoder de, Point& p)
{
	decoder_struct_begin(de, "Point");
		decoder_field(de, "v", p.v);
	decoder_struct_end(de);
}

struct Triangle
{
	Point a, b, c;
};

inline static void
encode_struct(Encoder en, const Triangle& t)
{
	encoder_struct_begin(en, "Triangle");
		encoder_field(en, "a", t.a);
		encoder_field(en, "b", t.b);
		encoder_field(en, "c", t.c);
	encoder_struct_end(en);
}

inline static void
decode_struct(Decoder de, Triangle& t)
{
	decoder_struct_begin(de, "Triangle");
		decoder_field(de, "a", t.a);
		decoder_field(de, "b", t.b);
		decoder_field(de, "c", t.c);
	decoder_struct_end(de);
}

TEST_CASE("encoding decoding", "[fiz]")
{
	Triangle t { Point{0.0f, 1.0f}, Point{2.0f, 3.0f}, Point{4.0f, 5.0f}};

	Encoder e = encoder_new();
	encode(e, t);

	Decoder d = decoder_new(encoder_fiz_extract(e));
	Triangle t2{};
	decode(d, t2);

	decoder_free(d);
	encoder_free(e);

	CHECK(t.a.v == t2.a.v);
	CHECK(t.b.v == t2.b.v);
	CHECK(t.c.v == t2.c.v);
}
