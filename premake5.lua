mn = path.getabsolute("external/mn")
hml = path.getabsolute("external/hamilton")

workspace "fiz"
	configurations {"debug", "release"}
	platforms {"x86", "x64"}
	location "build"
	targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}/"
	startproject "unittest"
	defaultplatform "x64"

	group "External"
		include "external/mn/mn"
		include "external/hamilton/hml"

	group ""

	include "fiz"
	include "unittest"