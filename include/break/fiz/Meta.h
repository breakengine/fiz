#pragma once

#include <type_traits>

#include <stdint.h>

namespace brk::fiz
{
	template<typename T, typename U>
	inline constexpr bool type_equal =
		std::is_same_v<std::remove_cv_t<std::decay_t<T>>,
					   std::remove_cv_t<std::decay_t<U>>>;

	template<typename T>
	inline constexpr bool signed_int =
		type_equal<T, int8_t> || type_equal<T, int16_t> ||
		type_equal<T, int32_t> || type_equal<T, int64_t>;

	template<typename T>
	inline constexpr bool unsigned_int =
		type_equal<T, uint8_t> || type_equal<T, uint16_t> ||
		type_equal<T, uint32_t> || type_equal<T, uint64_t>;

	template<typename T>
	inline constexpr bool floating = type_equal<T, float> || type_equal<T, double>;

	template<typename T>
	inline constexpr bool string_literal = type_equal<T, const char*> || type_equal<T, char*>;

	template<typename T>
	inline constexpr bool string = string_literal<T> || type_equal<T, mn::Str>;
}