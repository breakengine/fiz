#pragma once

#include "break/fiz/Exports.h"

#include <mn/Memory_Stream.h>
#include <mn/Str.h>

#include <hml/Vector.h>
#include <hml/Matrix.h>
#include <hml/Quaternion.h>

#include <stddef.h>
#include <assert.h>

namespace brk::fiz
{
	using Buffer = mn::Memory_Stream;

	inline static Buffer
	buffer_new()
	{
		return mn::memory_stream_new();
	}

	inline static void
	buffer_free(Buffer& self)
	{
		mn::memory_stream_free(self);
	}

	inline static void
	destruct(Buffer& self)
	{
		buffer_free(self);
	}

	inline static bool
	buffer_eof(Buffer& self)
	{
		return self.cursor == mn::memory_stream_size(self);
	}


	//push
	inline static void
	buffer_push8(Buffer& self, uint8_t v)
	{
		size_t w = mn::memory_stream_write(self, mn::block_from(v));
		assert(w == sizeof(v));
	}

	inline static void
	buffer_push16(Buffer& self, uint16_t v)
	{
		size_t w = mn::memory_stream_write(self, mn::block_from(v));
		assert(w == sizeof(v));
	}

	inline static void
	buffer_push32(Buffer& self, uint32_t v)
	{
		size_t w = mn::memory_stream_write(self, mn::block_from(v));
		assert(w == sizeof(v));
	}

	inline static void
	buffer_push64(Buffer& self, uint64_t v)
	{
		size_t w = mn::memory_stream_write(self, mn::block_from(v));
		assert(w == sizeof(v));
	}

	API_FIZ void
	buffer_push_uint(Buffer& self, uint64_t v);

	API_FIZ void
	buffer_push_int(Buffer& self, int64_t v);

	API_FIZ void
	buffer_push_bool(Buffer& self, bool v);

	API_FIZ void
	buffer_push_float(Buffer& self, double v);

	API_FIZ void
	buffer_push_str(Buffer& self, const mn::Str& v);

	inline static void
	buffer_push_str(Buffer& self, const char* v)
	{
		buffer_push_str(self, mn::str_lit(v));
	}

	API_FIZ void
	buffer_push_byte_array(Buffer& self, const mn::Block& bytes);

	API_FIZ void
	buffer_push_vec2i(Buffer& self, const hml::vec2i& v);

	API_FIZ void
	buffer_push_vec3i(Buffer& self, const hml::vec3i& v);

	API_FIZ void
	buffer_push_vec4i(Buffer& self, const hml::vec4i& v);

	API_FIZ void
	buffer_push_vec2f(Buffer& self, const hml::vec2f& v);

	API_FIZ void
	buffer_push_vec3f(Buffer& self, const hml::vec3f& v);

	API_FIZ void
	buffer_push_vec4f(Buffer& self, const hml::vec4f& v);

	API_FIZ void
	buffer_push_mat3f(Buffer& self, const hml::mat3f& v);

	API_FIZ void
	buffer_push_mat4f(Buffer& self, const hml::mat4f& v);

	API_FIZ void
	buffer_push_quatf(Buffer& self, const hml::quatf& v);


	//pop
	inline static uint8_t
	buffer_pop8(Buffer& self)
	{
		uint8_t v;
		size_t r = mn::memory_stream_read(self, mn::block_from(v));
		assert(r == sizeof(v));
		return v;
	}

	inline static uint16_t
	buffer_pop16(Buffer& self)
	{
		uint16_t v;
		size_t r = mn::memory_stream_read(self, mn::block_from(v));
		assert(r == sizeof(v));
		return v;
	}

	inline static uint32_t
	buffer_pop32(Buffer& self)
	{
		uint32_t v;
		size_t r = mn::memory_stream_read(self, mn::block_from(v));
		assert(r == sizeof(v));
		return v;
	}

	inline static uint64_t
	buffer_pop64(Buffer& self)
	{
		uint64_t v;
		size_t r = mn::memory_stream_read(self, mn::block_from(v));
		assert(r == sizeof(v));
		return v;
	}

	API_FIZ uint64_t
	buffer_pop_uint(Buffer& self);

	API_FIZ int64_t
	buffer_pop_int(Buffer& self);

	API_FIZ bool
	buffer_pop_bool(Buffer& self);

	API_FIZ double
	buffer_pop_float(Buffer& self);

	API_FIZ mn::Str
	buffer_pop_str(Buffer& self, mn::Allocator allocator = mn::allocator_top());

	API_FIZ mn::Buf<uint8_t>
	buffer_pop_byte_array(Buffer& self, mn::Allocator allocator = mn::allocator_top());

	API_FIZ hml::vec2i
	buffer_pop_vec2i(Buffer& self);

	API_FIZ hml::vec3i
	buffer_pop_vec3i(Buffer& self);

	API_FIZ hml::vec4i
	buffer_pop_vec4i(Buffer& self);

	API_FIZ hml::vec2f
	buffer_pop_vec2f(Buffer& self);

	API_FIZ hml::vec3f
	buffer_pop_vec3f(Buffer& self);

	API_FIZ hml::vec4f
	buffer_pop_vec4f(Buffer& self);

	API_FIZ hml::mat3f
	buffer_pop_mat3f(Buffer& self);

	API_FIZ hml::mat4f
	buffer_pop_mat4f(Buffer& self);

	API_FIZ hml::quatf
	buffer_pop_quatf(Buffer& self);
}
