#pragma once

#include "break/fiz/Buffer.h"

namespace brk::fiz
{
	struct Fiz
	{
		Buffer data;
		Buffer types;
	};

	inline static Fiz
	fiz_new()
	{
		return Fiz{
			buffer_new(),
			buffer_new()
		};
	}

	inline static Fiz
	fiz_from_buffers(const Buffer& data, const Buffer& types)
	{
		return Fiz{ data, types };
	}

	inline static void
	fiz_free(Fiz& self)
	{
		buffer_free(self.data);
		buffer_free(self.types);
	}

	inline static void
	destruct(Fiz& self)
	{
		fiz_free(self);
	}
}
