#pragma once

#include "break/fiz/Exports.h"
#include "break/fiz/Buffer.h"
#include "break/fiz/Type.h"
#include "break/fiz/Meta.h"
#include "break/fiz/Fiz.h"

#include <mn/Base.h>
#include <mn/Str.h>

namespace brk::fiz
{
	MS_HANDLE(Decoder);

	API_FIZ Decoder
	decoder_new(const Fiz& fiz);

	API_FIZ void
	decoder_free(Decoder decoder);

	inline static void
	destruct(Decoder decoder)
	{
		decoder_free(decoder);
	}

	API_FIZ TYPE
	decoder_peek(Decoder decoder);

	API_FIZ uint64_t
	decode_uint(Decoder decoder);

	API_FIZ int64_t
	decode_int(Decoder decoder);

	API_FIZ bool
	decode_bool(Decoder decoder);

	API_FIZ double
	decode_float(Decoder decoder);

	API_FIZ mn::Str
	decode_str(Decoder decoder, mn::Allocator allocator = mn::allocator_top());

	API_FIZ mn::Buf<uint8_t>
	decoder_byte_array(Decoder decoder, mn::Allocator allocator = mn::allocator_top());

	API_FIZ hml::vec2i
	decode_vec2i(Decoder decoder);

	API_FIZ hml::vec3i
	decode_vec3i(Decoder decoder);

	API_FIZ hml::vec4i
	decode_vec4i(Decoder decoder);

	API_FIZ hml::vec2f
	decode_vec2f(Decoder decoder);

	API_FIZ hml::vec3f
	decode_vec3f(Decoder decoder);

	API_FIZ hml::vec4f
	decode_vec4f(Decoder decoder);

	API_FIZ hml::mat3f
	decode_mat3f(Decoder decoder);

	API_FIZ hml::mat4f
	decode_mat4f(Decoder decoder);

	API_FIZ hml::quatf
	decode_quatf(Decoder decoder);

	template<typename T>
	inline static void
	decode_struct(Decoder decoder, const T& v)
	{
		static_assert(sizeof(T) == 0, "decode_struct overload is not defined for this type");
	}

	template<typename T>
	inline static void
	decode(Decoder decoder, T& v)
	{
		if constexpr(unsigned_int<T>)
		{
			v = T(decode_uint(decoder));
		}
		else if constexpr(signed_int<T>)
		{
			v = T(decode_int(decoder, v));
		}
		else if constexpr(type_equal<T, bool>)
		{
			v = T(decode_bool(decoder));
		}
		else if constexpr(floating<T>)
		{
			v = T(decode_float(decoder));
		}
		else if constexpr(type_equal<T, mn::Str>)
		{
			v = decode_str(decoder, v.allocator);
		}
		else if constexpr(type_equal<T, mn::Buf<uint8_t>>)
		{
			v = decoder_byte_array(decoder, v.allocator);
		}
		else if constexpr(type_equal<T, hml::vec2i>)
		{
			v = decode_vec2i(decoder);
		}
		else if constexpr(type_equal<T, hml::vec3i>)
		{
			v = decode_vec3i(decoder);
		}
		else if constexpr(type_equal<T, hml::vec4i>)
		{
			v = decode_vec4i(decoder);
		}
		else if constexpr(type_equal<T, hml::vec2f>)
		{
			v = decode_vec2f(decoder);
		}
		else if constexpr(type_equal<T, hml::vec3f>)
		{
			v = decode_vec3f(decoder);
		}
		else if constexpr(type_equal<T, hml::vec4f>)
		{
			v = decode_vec4f(decoder);
		}
		else if constexpr(type_equal<T, hml::mat3f>)
		{
			v = decode_mat3f(decoder);
		}
		else if constexpr(type_equal<T, hml::mat4f>)
		{
			v = decode_mat4f(decoder);
		}
		else if constexpr(type_equal<T, hml::quatf>)
		{
			v = decode_quatf(decoder);
		}
		else
		{
			decode_struct(decoder, v);
		}
	}

	API_FIZ void
	decoder_struct_begin(Decoder decoder, const mn::Str& name);

	inline static void
	decoder_struct_begin(Decoder decoder, const char* name)
	{
		decoder_struct_begin(decoder, mn::str_lit(name));
	}

	API_FIZ void
	decoder_struct_end(Decoder decoder);

	API_FIZ void
	_decoder_struct_seek(Decoder decoder);

	API_FIZ void
	_decoder_field_seek(Decoder decoder, uint64_t ix);

	API_FIZ uint64_t
	_decoder_struct_field_index(Decoder decoder, const mn::Str& name);

	API_FIZ Buffer&
	_decoder_buffer(Decoder decoder);

	inline static uint64_t
	_decoder_struct_field_index(Decoder decoder, const char* name)
	{
		return _decoder_struct_field_index(decoder, mn::str_lit(name));
	}

	template<typename T>
	inline static void
	decoder_field(Decoder decoder, const mn::Str& name, T& v)
	{
		uint64_t index = _decoder_struct_field_index(decoder, name);
		_decoder_field_seek(decoder, index);
		if constexpr(unsigned_int<T>)
		{
			v = T(buffer_pop_uint(_decoder_buffer(decoder)));
		}
		else if constexpr(signed_int<T>)
		{
			v = T(buffer_pop_int(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, bool>)
		{
			v = T(buffer_pop_bool(_decoder_buffer(decoder)));
		}
		else if constexpr(floating<T>)
		{
			v = T(buffer_pop_float(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, mn::Str>)
		{
			v = T(buffer_pop_str(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, mn::Buf<uint8_t>>)
		{
			v = T(buffer_pop_byte_array(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::vec2i>)
		{
			v = T(buffer_pop_vec2i(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::vec3i>)
		{
			v = T(buffer_pop_vec3i(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::vec4i>)
		{
			v = T(buffer_pop_vec4i(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::vec2f>)
		{
			v = T(buffer_pop_vec2f(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::vec3f>)
		{
			v = T(buffer_pop_vec3f(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::vec4f>)
		{
			v = T(buffer_pop_vec4f(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::mat3f>)
		{
			v = T(buffer_pop_mat3f(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::mat4f>)
		{
			v = T(buffer_pop_mat4f(_decoder_buffer(decoder)));
		}
		else if constexpr(type_equal<T, hml::quatf>)
		{
			v = T(buffer_pop_quatf(_decoder_buffer(decoder)));
		}
		else
		{
			decode(decoder, v);
		}
		_decoder_struct_seek(decoder);
	}

	template<typename T>
	inline static void
	decoder_field(Decoder decoder, const char* name, T& v)
	{
		decoder_field(decoder, mn::str_lit(name), v);
	}
}
