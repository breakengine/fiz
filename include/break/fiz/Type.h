#pragma once

#include "break/fiz/Buffer.h"

#include <mn/Buf.h>
#include <mn/Str_Intern.h>
#include <mn/Pool.h>

#include <stdint.h>
#include <stddef.h>

namespace brk::fiz
{
	enum class TYPE
	{
		NONE,
		UINT,
		INT,
		BOOL,
		FLOAT,
		STRING,
		BYTE_ARRAY,
		VEC2I,
		VEC3I,
		VEC4I,
		VEC2F,
		VEC3F,
		VEC4F,
		MAT3F,
		MAT4F,
		QUATF,
		FIELD,
		STRUCT,
		COUNT
	};


	//type support
	struct Field
	{
		const char* name;
		uint64_t type_id;
	};

	struct Struct
	{
		const char* name;
		uint64_t type_id;
		mn::Buf<Field> fields;
	};

	struct Type_Table
	{
		mn::Str_Intern str_table;
		mn::Map<const char*, Struct*> struct_table;
		mn::Pool struct_pool;
		uint64_t type_id_counter;
	};

	inline static Type_Table
	type_table_new()
	{
		Type_Table self{};
		self.str_table = mn::str_intern_new();
		self.struct_table = mn::map_new<const char*, Struct*>();
		self.struct_pool = mn::pool_new(sizeof(Struct), 64);
		self.type_id_counter = uint64_t(TYPE::COUNT);
		return self;
	}

	inline static void
	type_table_free(Type_Table& self)
	{
		for(auto it = mn::map_begin(self.struct_table);
			it != mn::map_end(self.struct_table);
			it = mn::map_next(self.struct_table, it))
		{
			mn::buf_free(it->value->fields);
		}

		mn::str_intern_free(self.str_table);
		mn::map_free(self.struct_table);
		mn::pool_free(self.struct_pool);
	}

	inline static bool
	type_table_struct_exists(Type_Table& self, const mn::Str& name)
	{
		const char* interned_name = mn::str_intern(self.str_table, name);
		if(auto it = mn::map_lookup(self.struct_table, interned_name))
			return true;
		return false;
	}

	inline static bool
	type_table_struct_exists(Type_Table& self, const char* name)
	{
		return type_table_struct_exists(self, mn::str_lit(name));
	}

	inline static Struct*
	type_table_struct(Type_Table& self, const mn::Str& name)
	{
		const char* interned_name = mn::str_intern(self.str_table, name);
		if(auto it = mn::map_lookup(self.struct_table, interned_name))
			return it->value;

		Struct* s = (Struct*)mn::pool_get(self.struct_pool);
		s->name = interned_name;
		s->type_id = self.type_id_counter++;
		s->fields = mn::buf_new<Field>();
		mn::map_insert(self.struct_table, s->name, s);
		return s;
	}

	inline static Struct*
	type_table_struct(Type_Table& self, const char* name)
	{
		return type_table_struct(self, mn::str_lit(name));
	}

	inline static bool
	type_table_struct_field_exists(Type_Table& self, Struct* s, const mn::Str& name)
	{
		const char* interned_name = mn::str_intern(self.str_table, name);
		for(const Field& f: s->fields)
			if(f.name == interned_name)
				return true;
		return false;
	}

	inline static bool
	type_table_struct_field_exists(Type_Table& self, Struct* s, const char* name)
	{
		return type_table_struct_field_exists(self, s, mn::str_lit(name));
	}

	inline static Field
	type_table_struct_field_lookup(Type_Table& self, Struct* s, const mn::Str& name)
	{
		const char* interned_name = mn::str_intern(self.str_table, name);
		for(const Field& f: s->fields)
			if(f.name == interned_name)
				return f;
		return Field{};
	}

	inline static Field
	type_table_struct_field_lookup(Type_Table& self, Struct* s, const char* name)
	{
		return type_table_struct_field_lookup(self, s, mn::str_lit(name));
	}

	inline static uint64_t
	type_table_struct_field_index(Type_Table& self, Struct* s, const mn::Str& name)
	{
		const char* interned_name = mn::str_intern(self.str_table, name);
		uint64_t i = 0;
		for(i = 0; i < s->fields.count; ++i)
			if(s->fields[i].name == interned_name)
				return i;
		return i;
	}

	inline static void
	type_table_struct_field_push(Type_Table& self, Struct* s, const mn::Str& name, uint64_t type_id)
	{
		buf_push(s->fields, Field{
			mn::str_intern(self.str_table, name),
			type_id
		});
	}

	inline static void
	type_table_struct_field_push(Type_Table& self, Struct* s, const char* name, uint64_t type_id)
	{
		type_table_struct_field_push(self, s, mn::str_lit(name), type_id);
	}

	inline static void
	type_table_dump(Type_Table& self, Buffer& b)
	{
		for(auto it = mn::map_begin(self.struct_table);
			it != map_end(self.struct_table);
			it = map_next(self.struct_table, it))
		{
			buffer_push_uint(b, uint64_t(TYPE::STRUCT));
			buffer_push_uint(b, it->value->type_id);
			buffer_push_str(b, it->value->name);
			buffer_push_uint(b, it->value->fields.count);

			for(const Field& f: it->value->fields)
			{
				buffer_push_uint(b, uint64_t(TYPE::FIELD));
				buffer_push_uint(b, f.type_id);
				buffer_push_str(b, f.name);
			}
		}
	}

	inline static void
	type_table_fill(Type_Table& self, Buffer& b)
	{
		mn::Str tmp = mn::str_new();
		while(buffer_eof(b) == false)
		{
			uint64_t t = buffer_pop_uint(b);
			assert(t == uint64_t(TYPE::STRUCT));
			Struct* s = (Struct*)mn::pool_get(self.struct_pool);
			s->type_id = buffer_pop_uint(b);

			uint64_t len = buffer_pop_uint(b);
			mn::str_resize(tmp, len);
			mn::memory_stream_read(b, mn::block_from(tmp));
			s->name = mn::str_intern(self.str_table, tmp);

			uint64_t fields_count = buffer_pop_uint(b);
			s->fields = mn::buf_new<Field>();
			mn::buf_resize(s->fields, fields_count);

			for(size_t i = 0; i < fields_count; ++i)
			{
				uint64_t t = buffer_pop_uint(b);
				assert(t == uint64_t(TYPE::FIELD));
				s->fields[i].type_id = buffer_pop_uint(b);

				uint64_t len = buffer_pop_uint(b);
				mn::str_resize(tmp, len);
				mn::memory_stream_read(b, mn::block_from(tmp));
				s->fields[i].name = mn::str_intern(self.str_table, tmp);
			}
			mn::map_insert(self.struct_table, s->name, s);
		}
		mn::str_free(tmp);
	}
}
