#pragma once

#if defined(OS_WINDOWS)
	#if defined(FIZ_DLL)
		#define API_FIZ __declspec(dllexport)
	#else
		#define API_FIZ __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_FIZ 
#endif