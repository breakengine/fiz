#pragma once

#if OS_WINDOWS
#include <intrin.h>
#include <stdlib.h>
#endif

#include <stdint.h>

namespace brk::fiz
{
	#if OS_WINDOWS

	inline static int
	leading_zeros(uint64_t v)
	{
		size_t bit_size = sizeof(uint64_t) * 8;
		unsigned long res = 0;
		if(_BitScanReverse64(&res, v))
			return bit_size - res;
		else
			return bit_size;
	}

	inline static uint64_t
	byte_swap64(uint64_t v)
	{
		return _byteswap_uint64(v);
	}

	#elif OS_LINUX

	inline static int
	leading_zeros(uint64_t v)
	{
		return __builtin_clzl(v);
	}

	inline static uint64_t
	byte_swap64(uint64_t v)
	{
		return __builtin_bswap64(v);
	}

	#endif
}
