#pragma once

#include "break/fiz/Exports.h"
#include "break/fiz/Meta.h"
#include "break/fiz/Type.h"
#include "break/fiz/Buffer.h"
#include "break/fiz/Fiz.h"

#include <mn/Base.h>
#include <mn/Str.h>

namespace brk::fiz
{
	MS_HANDLE(Encoder);

	API_FIZ Encoder
	encoder_new();

	API_FIZ void
	encoder_free(Encoder encoder);

	inline static void
	destruct(Encoder encoder)
	{
		encoder_free(encoder);
	}

	API_FIZ Fiz
	encoder_fiz_extract(Encoder encoder);

	API_FIZ void
	encode_uint(Encoder encoder, uint64_t v);

	API_FIZ void
	encode_int(Encoder encoder, int64_t v);

	API_FIZ void
	encode_bool(Encoder encoder, bool v);

	API_FIZ void
	encode_float(Encoder encoder, double v);

	API_FIZ void
	encode_str(Encoder encoder, const mn::Str& v);

	inline static void
	encode_str(Encoder encoder, const char* v)
	{
		encode_str(encoder, mn::str_lit(v));
	}

	API_FIZ void
	encode_byte_array(Encoder encoder, const mn::Block& bytes);

	API_FIZ void
	encode_vec2i(Encoder encoder, const hml::vec2i& v);

	API_FIZ void
	encode_vec3i(Encoder encoder, const hml::vec3i& v);

	API_FIZ void
	encode_vec4i(Encoder encoder, const hml::vec4i& v);

	API_FIZ void
	encode_vec2f(Encoder encoder, const hml::vec2f& v);

	API_FIZ void
	encode_vec3f(Encoder encoder, const hml::vec3f& v);

	API_FIZ void
	encode_vec4f(Encoder encoder, const hml::vec4f& v);

	API_FIZ void
	encode_mat3f(Encoder encoder, const hml::mat3f& v);

	API_FIZ void
	encode_mat4f(Encoder encoder, const hml::mat4f& v);

	API_FIZ void
	encode_quatf(Encoder encoder, const hml::quatf& v);

	template<typename T>
	inline static void
	encode_struct(Encoder encoder, const T& v)
	{
		static_assert(sizeof(T) == 0, "encode_struct overload is not defined for this type");
	}

	API_FIZ void
	encoder_struct_begin(Encoder encoder, const mn::Str& name);

	inline static void
	encoder_struct_begin(Encoder encoder, const char* name)
	{
		encoder_struct_begin(encoder, mn::str_lit(name));
	}

	template<typename T>
	inline static void
	encode(Encoder encoder, const T& v)
	{
		if constexpr(unsigned_int<T>)
		{
			encode_uint(encoder, v);
		}
		else if constexpr(signed_int<T>)
		{
			encode_int(encoder, v);
		}
		else if constexpr(type_equal<T, bool>)
		{
			encode_bool(encoder, v);
		}
		else if constexpr(floating<T>)
		{
			encode_float(encoder, v);
		}
		else if constexpr(string<T>)
		{
			encode_str(encoder, v);
		}
		else if constexpr(type_equal<T, mn::Block>)
		{
			encode_byte_array(encoder, v);
		}
		else if constexpr(type_equal<T, hml::vec2i>)
		{
			encode_vec2i(encoder, v);
		}
		else if constexpr(type_equal<T, hml::vec3i>)
		{
			encode_vec3i(encoder, v);
		}
		else if constexpr(type_equal<T, hml::vec4i>)
		{
			encode_vec4i(encoder, v);
		}
		else if constexpr(type_equal<T, hml::vec2f>)
		{
			encode_vec2f(encoder, v);
		}
		else if constexpr(type_equal<T, hml::vec3f>)
		{
			encode_vec3f(encoder, v);
		}
		else if constexpr(type_equal<T, hml::vec4f>)
		{
			encode_vec4f(encoder, v);
		}
		else if constexpr(type_equal<T, hml::mat3f>)
		{
			encode_mat3f(encoder, v);
		}
		else if constexpr(type_equal<T, hml::mat4f>)
		{
			encode_mat4f(encoder, v);
		}
		else if constexpr(type_equal<T, hml::quatf>)
		{
			encode_quatf(encoder, v);
		}
		else
		{
			encode_struct(encoder, v);
		}
	}

	API_FIZ void
	encoder_struct_field(Encoder encoder, const mn::Str& name, uint64_t type);

	API_FIZ void
	encoder_struct_end(Encoder encoder);

	API_FIZ void
	_encoder_buffer_fork(Encoder encoder);

	API_FIZ void
	_encoder_buffer_merge(Encoder encoder, bool write_size);

	API_FIZ Buffer&
	_encoder_buffer(Encoder encoder);

	template<typename T>
	inline static void
	encoder_field(Encoder encoder, const mn::Str& name, const T& v)
	{
		if constexpr(unsigned_int<T>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::UINT));
			_encoder_buffer_fork(encoder);
				buffer_push_uint(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(signed_int<T>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::INT));
			_encoder_buffer_fork(encoder);
				buffer_push_int(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, bool>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::BOOL));
			_encoder_buffer_fork(encoder);
				buffer_push_bool(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(floating<T>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::FLOAT));
			_encoder_buffer_fork(encoder);
				buffer_push_float(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(string<T>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::STRING));
			_encoder_buffer_fork(encoder);
				buffer_push_str(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, mn::Block>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::BYTE_ARRAY));
			_encoder_buffer_fork(encoder);
				buffer_push_byte_array(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::vec2i>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::VEC2I));
			_encoder_buffer_fork(encoder);
				buffer_push_vec2i(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::vec3i>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::VEC3I));
			_encoder_buffer_fork(encoder);
				buffer_push_vec3i(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::vec4i>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::VEC4I));
			_encoder_buffer_fork(encoder);
				buffer_push_vec4i(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::vec2f>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::VEC2F));
			_encoder_buffer_fork(encoder);
				buffer_push_vec2f(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::vec3f>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::VEC3F));
			_encoder_buffer_fork(encoder);
				buffer_push_vec3f(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::vec4f>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::VEC4F));
			_encoder_buffer_fork(encoder);
				buffer_push_vec4f(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::mat3f>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::MAT3F));
			_encoder_buffer_fork(encoder);
				buffer_push_mat3f(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::mat4f>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::MAT4F));
			_encoder_buffer_fork(encoder);
				buffer_push_mat4f(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else if constexpr(type_equal<T, hml::quatf>)
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::QUATF));
			_encoder_buffer_fork(encoder);
				buffer_push_quatf(_encoder_buffer(encoder), v);
			_encoder_buffer_merge(encoder, true);
		}
		else
		{
			encoder_struct_field(encoder, name, uint64_t(TYPE::FIELD));
			_encoder_buffer_fork(encoder);
				encode_struct(encoder, v);
			_encoder_buffer_merge(encoder, true);
		}
	}

	template<typename T>
	inline static void
	encoder_field(Encoder encoder, const char* name, const T& v)
	{
		encoder_field(encoder, mn::str_lit(name), v);
	}
}
