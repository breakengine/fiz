#include "break/fiz/Buffer.h"
#include "break/fiz/Bits.h"

namespace brk::fiz
{
	using namespace mn;

	//push
	void
	buffer_push_uint(Buffer& self, uint64_t v)
	{
		if(v < 128)
		{
			buffer_push8(self, (uint8_t)v);
			return;
		}

		int zero_bytes = leading_zeros(v) / 8;
		//we write the size of the integer in bytes using a negative sign
		//so for example 512 will write -2 since it uses 2 bytes
		buffer_push8(self, zero_bytes - sizeof(v));

		Block block{ &v, sizeof(v) - zero_bytes };
		size_t w = memory_stream_write(self, block);
		assert(w == block.size);
	}

	void
	buffer_push_int(Buffer& self, int64_t v)
	{
		uint64_t x = 0;
		//move the sign bit to the front
		if(v < 0)
			x = uint64_t(~v << 1) | 1;
		else
			x = uint64_t(v << 1);
		buffer_push_uint(self, x);
	}

	void
	buffer_push_bool(Buffer& self, bool v)
	{
		buffer_push_uint(self, v ? 1 : 0);
	}

	void
	buffer_push_float(Buffer& self, double v)
	{
		buffer_push_uint(self, byte_swap64(*(uint64_t*)&v));
	}

	void
	buffer_push_str(Buffer& self, const Str& v)
	{
		buffer_push_uint(self, uint64_t(v.count));
		size_t w = memory_stream_write(self, mn::block_from(v));
		assert(w == v.count);
	}

	void
	buffer_push_byte_array(Buffer& self, const mn::Block& bytes)
	{
		buffer_push_uint(self, uint64_t(bytes.size));
		size_t w = memory_stream_write(self, bytes);
		assert(w == bytes.size);
	}

	void
	buffer_push_vec2i(Buffer& self, const hml::vec2i& v)
	{
		buffer_push_uint(self, v.x);
		buffer_push_uint(self, v.y);
	}

	void
	buffer_push_vec3i(Buffer& self, const hml::vec3i& v)
	{
		buffer_push_uint(self, v.x);
		buffer_push_uint(self, v.y);
		buffer_push_uint(self, v.z);
	}

	void
	buffer_push_vec4i(Buffer& self, const hml::vec4i& v)
	{
		buffer_push_uint(self, v.x);
		buffer_push_uint(self, v.y);
		buffer_push_uint(self, v.z);
		buffer_push_uint(self, v.w);
	}

	void
	buffer_push_vec2f(Buffer& self, const hml::vec2f& v)
	{
		buffer_push_float(self, v.x);
		buffer_push_float(self, v.y);
	}

	void
	buffer_push_vec3f(Buffer& self, const hml::vec3f& v)
	{
		buffer_push_float(self, v.x);
		buffer_push_float(self, v.y);
		buffer_push_float(self, v.z);
	}

	void
	buffer_push_vec4f(Buffer& self, const hml::vec4f& v)
	{
		buffer_push_float(self, v.x);
		buffer_push_float(self, v.y);
		buffer_push_float(self, v.z);
		buffer_push_float(self, v.w);
	}

	void
	buffer_push_mat3f(Buffer& self, const hml::mat3f& v)
	{
		buffer_push_vec3f(self, v[0]);
		buffer_push_vec3f(self, v[1]);
		buffer_push_vec3f(self, v[2]);
	}

	void
	buffer_push_mat4f(Buffer& self, const hml::mat4f& v)
	{
		buffer_push_vec4f(self, v[0]);
		buffer_push_vec4f(self, v[1]);
		buffer_push_vec4f(self, v[2]);
		buffer_push_vec4f(self, v[3]);
	}

	void
	buffer_push_quatf(Buffer& self, const hml::quatf& v)
	{
		buffer_push_float(self, v.w);
		buffer_push_float(self, v.x);
		buffer_push_float(self, v.y);
		buffer_push_float(self, v.z);
	}


	//pop
	uint64_t
	buffer_pop_uint(Buffer& self)
	{
		int8_t res = buffer_pop8(self);
		//this is a single byte uint
		if(res >= 0)
			return res;

		if(res == -1)
		{
			return buffer_pop8(self);
		}
		else if(res == -2)
		{
			return buffer_pop16(self);
		}
		else if(res == -3 || res == -4)
		{
			uint32_t v = 0;
			Block b{&v, res*-1};
			size_t w = memory_stream_read(self, b);
			assert(w == b.size);
			return v;
		}
		else if(res == -5 || res == -6 || res == -7 || res == -8)
		{
			uint64_t v = 0;
			Block b{&v, res*-1};
			size_t w = memory_stream_read(self, b);
			assert(w == b.size);
			return v;
		}
		else
		{
			assert(false && "uint overflow");
			return 0;
		}
	}

	int64_t
	buffer_pop_int(Buffer& self)
	{
		uint64_t x = buffer_pop_uint(self);
		//reverse the sign bit
		if(x & 1)
			return ~int64_t(x >> 1);
		return int64_t(x >> 1);
	}

	bool
	buffer_pop_bool(Buffer& self)
	{
		return (bool)buffer_pop_uint(self);
	}

	double
	buffer_pop_float(Buffer& self)
	{
		uint64_t v = byte_swap64(buffer_pop_uint(self));
		return *(double*)&v;
	}

	Str
	buffer_pop_str(Buffer& self, Allocator allocator)
	{
		mn::Str v = str_with_allocator(allocator);
		uint64_t len = buffer_pop_uint(self);
		str_resize(v, len);
		size_t w = memory_stream_read(self, block_from(v));
		assert(w == len);
		return v;
	}

	mn::Buf<uint8_t>
	buffer_pop_byte_array(Buffer& self, mn::Allocator allocator)
	{
		Buf<uint8_t> v = buf_with_allocator<uint8_t>(allocator);
		uint64_t len = buffer_pop_uint(self);
		buf_resize(v, len);
		size_t w = memory_stream_read(self, block_from(v));
		assert(w == len);
		return v;
	}

	hml::vec2i
	buffer_pop_vec2i(Buffer& self)
	{
		hml::vec2i v{};
		v.x = buffer_pop_uint(self);
		v.y = buffer_pop_uint(self);
		return v;
	}

	hml::vec3i
	buffer_pop_vec3i(Buffer& self)
	{
		hml::vec3i v{};
		v.x = buffer_pop_uint(self);
		v.y = buffer_pop_uint(self);
		v.z = buffer_pop_uint(self);
		return v;
	}

	hml::vec4i
	buffer_pop_vec4i(Buffer& self)
	{
		hml::vec4i v{};
		v.x = buffer_pop_uint(self);
		v.y = buffer_pop_uint(self);
		v.z = buffer_pop_uint(self);
		v.w = buffer_pop_uint(self);
		return v;
	}

	hml::vec2f
	buffer_pop_vec2f(Buffer& self)
	{
		hml::vec2f v{};
		v.x = buffer_pop_float(self);
		v.y = buffer_pop_float(self);
		return v;
	}

	hml::vec3f
	buffer_pop_vec3f(Buffer& self)
	{
		hml::vec3f v{};
		v.x = buffer_pop_float(self);
		v.y = buffer_pop_float(self);
		v.z = buffer_pop_float(self);
		return v;
	}

	hml::vec4f
	buffer_pop_vec4f(Buffer& self)
	{
		hml::vec4f v{};
		v.x = buffer_pop_float(self);
		v.y = buffer_pop_float(self);
		v.z = buffer_pop_float(self);
		v.w = buffer_pop_float(self);
		return v;
	}

	hml::mat3f
	buffer_pop_mat3f(Buffer& self)
	{
		hml::mat3f m{};
		m[0] = buffer_pop_vec3f(self);
		m[1] = buffer_pop_vec3f(self);
		m[2] = buffer_pop_vec3f(self);
		return m;
	}

	hml::mat4f
	buffer_pop_mat4f(Buffer& self)
	{
		hml::mat4f m{};
		m[0] = buffer_pop_vec4f(self);
		m[1] = buffer_pop_vec4f(self);
		m[2] = buffer_pop_vec4f(self);
		m[3] = buffer_pop_vec4f(self);
		return m;
	}

	hml::quatf
	buffer_pop_quatf(Buffer& self)
	{
		hml::quatf q{};
		q.w = buffer_pop_float(self);
		q.x = buffer_pop_float(self);
		q.y = buffer_pop_float(self);
		q.z = buffer_pop_float(self);
		return q;
	}
}
