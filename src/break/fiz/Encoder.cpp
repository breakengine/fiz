#include "break/fiz/Encoder.h"

#include <mn/Memory.h>

namespace brk::fiz
{
	using namespace mn;

	struct IEncoder
	{
		Buf<Buffer> data_stack;
		Type_Table types;
		Buf<Struct*> type_stack;
	};

	Encoder
	encoder_new()
	{
		IEncoder* self = alloc<IEncoder>();
		self->data_stack = buf_new<Buffer>();
		self->types = type_table_new();
		self->type_stack = buf_new<Struct*>();

		buf_push(self->data_stack, buffer_new());
		return (Encoder)self;
	}

	void
	encoder_free(Encoder encoder)
	{
		IEncoder* self = (IEncoder*)encoder;
		destruct(self->data_stack);
		type_table_free(self->types);
		buf_free(self->type_stack);
		free(self);
	}

	Fiz
	encoder_fiz_extract(Encoder encoder)
	{
		IEncoder* self = (IEncoder*)encoder;

		Buffer types = buffer_new();
		type_table_dump(self->types, types);
		Fiz res = fiz_from_buffers(buf_top(self->data_stack), types);

		buf_pop(self->data_stack);
		buf_push(self->data_stack, buffer_new());

		memory_stream_cursor_to_start(res.data);
		memory_stream_cursor_to_start(res.types);
		return res;
	}

	void
	encode_types(Encoder encoder, Buffer& b)
	{
		IEncoder* self = (IEncoder*)encoder;
		type_table_dump(self->types, b);
	}

	void
	encode_uint(Encoder encoder, uint64_t v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::UINT));
		buffer_push_uint(buf_top(self->data_stack), v);
	}

	void
	encode_int(Encoder encoder, int64_t v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::INT));
		buffer_push_int(buf_top(self->data_stack), v);
	}

	void
	encode_bool(Encoder encoder, bool v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::BOOL));
		buffer_push_bool(buf_top(self->data_stack), v);
	}

	void
	encode_float(Encoder encoder, double v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::FLOAT));
		buffer_push_float(buf_top(self->data_stack), v);
	}

	void
	encode_str(Encoder encoder, const mn::Str& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::STRING));
		buffer_push_str(buf_top(self->data_stack), v);
	}

	void
	encode_byte_array(Encoder encoder, const mn::Block& bytes)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::BYTE_ARRAY));
		buffer_push_byte_array(buf_top(self->data_stack), bytes);
	}

	void
	encode_vec2i(Encoder encoder, const hml::vec2i& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::VEC2I));
		buffer_push_vec2i(buf_top(self->data_stack), v);
	}

	void
	encode_vec3i(Encoder encoder, const hml::vec3i& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::VEC3I));
		buffer_push_vec3i(buf_top(self->data_stack), v);
	}

	void
	encode_vec4i(Encoder encoder, const hml::vec4i& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::VEC4I));
		buffer_push_vec4i(buf_top(self->data_stack), v);
	}

	void
	encode_vec2f(Encoder encoder, const hml::vec2f& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::VEC2F));
		buffer_push_vec2f(buf_top(self->data_stack), v);
	}

	void
	encode_vec3f(Encoder encoder, const hml::vec3f& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::VEC3F));
		buffer_push_vec3f(buf_top(self->data_stack), v);
	}

	void
	encode_vec4f(Encoder encoder, const hml::vec4f& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::VEC4F));
		buffer_push_vec4f(buf_top(self->data_stack), v);
	}

	void
	encode_mat3f(Encoder encoder, const hml::mat3f& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::MAT3F));
		buffer_push_mat3f(buf_top(self->data_stack), v);
	}

	void
	encode_mat4f(Encoder encoder, const hml::mat4f& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::MAT4F));
		buffer_push_mat4f(buf_top(self->data_stack), v);
	}

	void
	encode_quatf(Encoder encoder, const hml::quatf& v)
	{
		IEncoder* self = (IEncoder*)encoder;
		buffer_push_uint(buf_top(self->data_stack), uint64_t(TYPE::QUATF));
		buffer_push_quatf(buf_top(self->data_stack), v);
	}

	void
	encoder_struct_begin(Encoder encoder, const mn::Str& name)
	{
		IEncoder* self = (IEncoder*)encoder;
		Struct* s = type_table_struct(self->types, name);
		if(self->type_stack.count > 0)
		{
			Struct* prev_s = buf_top(self->type_stack);
			if(prev_s->fields.count > 0 && buf_top(prev_s->fields).type_id == uint64_t(TYPE::FIELD))
			{
				buf_top(prev_s->fields).type_id = s->type_id;
			}
		}
		buf_push(self->type_stack, s);
		buffer_push_uint(buf_top(self->data_stack), s->type_id);
	}

	void
	encoder_struct_field(Encoder encoder, const mn::Str& name, uint64_t type)
	{
		IEncoder* self = (IEncoder*)encoder;
		Struct* s = buf_top(self->type_stack);
		uint64_t ix = type_table_struct_field_index(self->types, s, name);
		if(ix == s->fields.count)
			type_table_struct_field_push(self->types, s, name, type);
		buffer_push_uint(buf_top(self->data_stack), ix);
	}

	void
	encoder_struct_end(Encoder encoder)
	{
		IEncoder* self = (IEncoder*)encoder;
		assert(self->type_stack.count > 0);
		buf_pop(self->type_stack);
	}

	void
	_encoder_buffer_fork(Encoder encoder)
	{
		IEncoder* self = (IEncoder*)encoder;
		buf_push(self->data_stack, buffer_new());
	}

	void
	_encoder_buffer_merge(Encoder encoder, bool write_size)
	{
		IEncoder* self = (IEncoder*)encoder;
		assert(self->data_stack.count > 1);
		Buffer b = buf_top(self->data_stack);
		buf_pop(self->data_stack);
		if(write_size)
			buffer_push_uint(buf_top(self->data_stack), uint64_t(memory_stream_size(b)));
		memory_stream_write(buf_top(self->data_stack), memory_stream_block_behind(b, 0));
		buffer_free(b);
	}

	Buffer&
	_encoder_buffer(Encoder encoder)
	{
		IEncoder* self = (IEncoder*)encoder;
		return buf_top(self->data_stack);
	}
}
