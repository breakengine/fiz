#include "break/fiz/Decoder.h"

#include <mn/Memory.h>

namespace brk::fiz
{
	using namespace mn;

	struct Stack_Frame
	{
		Struct* st;
		int64_t start;
		int64_t end;
		Buf<int64_t> fields;
	};

	inline static Stack_Frame
	stack_frame_new(Struct* st)
	{
		Stack_Frame self{};
		self.st = st;
		self.fields = buf_new<int64_t>();
		return self;
	}

	inline static void
	stack_frame_free(Stack_Frame& self)
	{
		buf_free(self.fields);
	}

	inline static void
	destruct(Stack_Frame& self)
	{
		stack_frame_free(self);
	}


	struct IDecoder
	{
		Fiz fiz;
		Type_Table types;
		Buf<Stack_Frame> stack;
	};

	Decoder
	decoder_new(const Fiz& fiz)
	{
		IDecoder* self = mn::alloc<IDecoder>();
		self->fiz = fiz;
		self->types = type_table_new();
		self->stack = buf_new<Stack_Frame>();
		type_table_fill(self->types, self->fiz.types);
		return (Decoder)self;
	}

	void
	decoder_free(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		fiz_free(self->fiz);
		type_table_free(self->types);
		destruct(self->stack);
		free(self);
	}

	TYPE
	decoder_peek(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		int64_t p = memory_stream_cursor_pos(self->fiz.data);
		TYPE res = TYPE(buffer_pop_uint(self->fiz.data));
		memory_stream_cursor_set(self->fiz.data, p);
		return res;
	}

	uint64_t
	decode_uint(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::UINT);
		return buffer_pop_uint(self->fiz.data);
	}

	int64_t
	decode_int(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::INT);
		return buffer_pop_int(self->fiz.data);
	}

	bool
	decode_bool(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::BOOL);
		return buffer_pop_bool(self->fiz.data);
	}

	double
	decode_float(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::FLOAT);
		return buffer_pop_float(self->fiz.data);
	}

	Str
	decode_str(Decoder decoder, mn::Allocator allocator)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::STRING);
		return buffer_pop_str(self->fiz.data, allocator);
	}

	mn::Buf<uint8_t>
	decoder_byte_array(Decoder decoder, mn::Allocator allocator)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::BYTE_ARRAY);
		return buffer_pop_byte_array(self->fiz.data, allocator);
	}

	hml::vec2i
	decode_vec2i(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::VEC2I);
		return buffer_pop_vec2i(self->fiz.data);
	}

	hml::vec3i
	decode_vec3i(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::VEC3I);
		return buffer_pop_vec3i(self->fiz.data);
	}

	hml::vec4i
	decode_vec4i(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::VEC4I);
		return buffer_pop_vec4i(self->fiz.data);
	}

	hml::vec2f
	decode_vec2f(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::VEC2F);
		return buffer_pop_vec2f(self->fiz.data);
	}

	hml::vec3f
	decode_vec3f(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::VEC3F);
		return buffer_pop_vec3f(self->fiz.data);
	}

	hml::vec4f
	decode_vec4f(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::VEC4F);
		return buffer_pop_vec4f(self->fiz.data);
	}

	hml::mat3f
	decode_mat3f(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::MAT3F);
		return buffer_pop_mat3f(self->fiz.data);
	}

	hml::mat4f
	decode_mat4f(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::MAT4F);
		return buffer_pop_mat4f(self->fiz.data);
	}

	hml::quatf
	decode_quatf(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		TYPE t = TYPE(buffer_pop_uint(self->fiz.data));
		assert(t == TYPE::QUATF);
		return buffer_pop_quatf(self->fiz.data);
	}

	void
	decoder_struct_begin(Decoder decoder, const mn::Str& name)
	{
		IDecoder* self = (IDecoder*)decoder;
		assert(type_table_struct_exists(self->types, name));

		Struct* s = type_table_struct(self->types, name);
		uint64_t id = buffer_pop_uint(self->fiz.data);
		assert(id == s->type_id && "decoder_struct_begin wrong struct id");

		Stack_Frame sf = stack_frame_new(s);
		sf.start = memory_stream_cursor_pos(self->fiz.data);
		buf_resize(sf.fields, sf.st->fields.count);
		for(size_t i = 0; i < sf.fields.count; ++i)
		{
			uint64_t id = buffer_pop_uint(self->fiz.data);
			assert(id < sf.fields.count);
			uint64_t size = buffer_pop_uint(self->fiz.data);
			assert(size <= INT64_MAX);
			sf.fields[id] = memory_stream_cursor_pos(self->fiz.data);
			memory_stream_cursor_move(self->fiz.data, size);
		}
		sf.end = memory_stream_cursor_pos(self->fiz.data);
		memory_stream_cursor_set(self->fiz.data, sf.start);

		buf_push(self->stack, sf);
	}

	void
	decoder_struct_end(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		assert(self->stack.count > 0);
		Stack_Frame sf = buf_top(self->stack);
		memory_stream_cursor_set(self->fiz.data, sf.end);
		buf_pop(self->stack);
		stack_frame_free(sf);
	}

	void
	_decoder_struct_seek(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		memory_stream_cursor_set(self->fiz.data, buf_top(self->stack).start);
	}

	void
	_decoder_field_seek(Decoder decoder, uint64_t ix)
	{
		IDecoder* self = (IDecoder*)decoder;
		const Stack_Frame& sf = buf_top(self->stack);
		assert(ix < sf.fields.count);
		memory_stream_cursor_set(self->fiz.data, sf.fields[ix]);
	}

	uint64_t
	_decoder_struct_field_index(Decoder decoder, const mn::Str& name)
	{
		IDecoder* self = (IDecoder*)decoder;

		const Stack_Frame& sf = buf_top(self->stack);
		return type_table_struct_field_index(self->types, sf.st, name);
	}

	Buffer&
	_decoder_buffer(Decoder decoder)
	{
		IDecoder* self = (IDecoder*)decoder;
		return self->fiz.data;
	}
}
